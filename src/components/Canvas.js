import React, { useRef, useEffect, useState } from 'react';
import {applyFilters} from "../helpers/filterFunctions.js";
import CameraswitchIcon from '@mui/icons-material/Cameraswitch';

let interval;
let vi = 1;

const Canvas = ({filters}) => {
    const main1 = useRef();
    const plainVideo = useRef();
    const filteredVideo = useRef();
    const downloadBtn = useRef();
    const recordBtn = useRef();

    const [alert, setAlert] = useState(null);
    const [camFront, setCamFront] = useState(true);
    const [orientation, setOrientation] = useState("");
    const [supportsFacingMode, setSupportsFacingMode] = useState(false);

    function flipCamera(){
        if (plainVideo.current.srcObject == null) return;
        setCamFront(!camFront);
    }

    //get video stream from device:
    useEffect(() => {
        let mediaApi = false;
        if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {
            mediaApi = true;
        }
        
        if (!mediaApi){
            setAlert("Error: app is not supported by this browser.");
            return;
        }

        const supports = navigator.mediaDevices.getSupportedConstraints();
        setSupportsFacingMode(supports['facingMode']);

        let options = {video: {facingMode: camFront ? 'user' : 'environment'}}

        //clear previous videostream:
        plainVideo.current.pause();
        plainVideo.current.srcObject = null;
        const ctx = filteredVideo.current.getContext('2d');
        ctx.clearRect(0, 0, filteredVideo.current.width, filteredVideo.current.height);
                            
        navigator.mediaDevices.getUserMedia(options)
            .then(localMediaStream => {
                plainVideo.current.srcObject = localMediaStream;
                plainVideo.current.play();
                plainVideo.current.style.transform = camFront ? "scaleX(-1)" : "none";
                filteredVideo.current.style.transform = camFront ? "scaleX(-1)" : "none";
            }).catch(err => {
                console.error(err);
                setAlert("Error: to use this app you need to allow it to access your camera.");
            });
    }, [camFront, orientation]);

    //apply filters and paint video to canvas:
    useEffect(() => {
        const ctx = filteredVideo.current.getContext('2d');
        const pvc = plainVideo.current;
        let pixels;
        function paintToCanvas(){
            const width = pvc.videoWidth * 2;
            const height = pvc.videoHeight * 2;
            [filteredVideo.current.width, filteredVideo.current.height] = [width, height];  

            const paintFrame = () => {
                ctx.drawImage(pvc, 0, 0, width, height);
                pixels = ctx.getImageData(0, 0, width, height);
                applyFilters(pixels, filters, ctx);
                ctx.putImageData(pixels, 0, 0);
            }

            paintFrame()
                
            clearInterval(interval);
            interval = setInterval(paintFrame, 20);
        };

        if (plainVideo.current.srcObject) {
            paintToCanvas();
        } else {
            pvc.addEventListener('canplay', paintToCanvas, {once: true});
        }

        return () => {
            clearInterval(interval);
        }
    }, [filters, camFront, orientation]);

    //setup event listeners for recording video from canvas:
    useEffect(() => {
        let chunks = [];
        let videoStream = filteredVideo.current.captureStream(30);

        /*The code below enables recording audio -  you would just need to add audio:true to
        navigator.mediaDevices.getUserMedia above. However, the audio quality is poor for
        laptops' built-in microphones.*/
        // let actx = new AudioContext();
        // let dest = actx.createMediaStreamDestination();
        // let sourceNode = actx.createMediaElementSource(plainVideo.current);    
        // sourceNode.connect(dest);
        // plainVideo.current.muted = true;
        // plainVideo.current.volume = 0;
        // let audioTrack = dest.stream.getAudioTracks()[0];
        // videoStream.addTrack(audioTrack);

        let mediaRecorder = new MediaRecorder(videoStream);
        mediaRecorder.ondataavailable = function(e) {
            chunks.push(e.data);
        };
        mediaRecorder.onstop = function(e) {
            let blob = new Blob(chunks, { type: 'video/mp4' });
            chunks = [];
            let videoURL = URL.createObjectURL(blob);
            downloadBtn.current.href = videoURL;
            if (vi > 1){
                downloadBtn.current.download = `webcam-filter-vid-${vi}`;
            }
            vi++
        };
        const record = (e) => {
            let btnText = e.target.innerText;
            if (btnText === "Record") {
                e.target.innerText = "Stop";
                e.target.classList.remove("record-btn");
                e.target.classList.add("stop-btn");
                mediaRecorder.start();
            } else {
                e.target.innerText = "Record";
                e.target.classList.remove("stop-btn");
                e.target.classList.add("record-btn");              
                mediaRecorder.stop();
                let download = downloadBtn.current.firstElementChild.classList;
                if (download.contains("hidden")) {
                    download.remove("hidden");
                }
                download.add("download");
                setTimeout(() => download.remove("download"), 1000);
                let dlNote = main1.current.querySelector("#download-note");
                dlNote.classList.remove("hidden");
            }       
        };
        recordBtn.current.addEventListener("click", record);
        let recordBtnRef = recordBtn.current;
        return () => {
            recordBtnRef.removeEventListener("click", record);
        };
    }, []);

    useEffect(() => {
        function changeOrientation(){
            if (plainVideo.current.srcObject == null) return;
            const orientation = (window.innerHeight > window.innerWidth) ? "portrait" : "landscape";
            setOrientation(orientation);
        }

        window.addEventListener("orientationchange", changeOrientation)

        return () => {
            window.removeEventListener("orientationchange", changeOrientation)
        }
    }, [])

    return (
        <main ref={main1}>
            {supportsFacingMode && <CameraswitchIcon className="cam-switch" onClick={flipCamera}/>}
            <section id="canvas-container">
                <canvas ref={filteredVideo}></canvas>
            </section>
            <section id="banner">
                <div style={{paddingLeft: "1rem"}}>
                    <p className="alert">{alert}</p>
                    <p><b>Note:</b> some filters may not work in all browsers.</p>
                    <div style={{display: "flex"}}>
                        <span>
                            <button type="button" ref={recordBtn} className="btn-base record-btn">Record</button>
                        </span>
                        {/* eslint-disable-next-line */}
                        <a href="" download="webcam-filter-vid" ref={downloadBtn}>
                            <button type="button" className="btn-base download-btn hidden">Download</button>
                        </a>
                    </div>
                    <p className="hidden" id="download-note">
                        Video downloads from here are in webm format. To convert to mp4 you can use a service like{" "}  
                        <a href="https://www.freeconvert.com/webm-to-mp4" target="_blank" rel="noopener noreferrer">this one</a>.
                    </p>
                </div>
                <video ref={plainVideo} autoPlay muted playsInline></video>
            </section>
        </main>
    )
}

export default Canvas;