import React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import InfoIcon from '@mui/icons-material/Info';
import Tooltip from '@mui/material/Tooltip';

const Filters = (props) => {
    const {filters, setFilters, reset} = props
    return (
        <aside>
            <p id="filter-title">Filters</p>
            <Tooltip arrow title="Reset all filters.">
                <button type="button" className="btn-base reset-btn" onClick={reset}>Reset</button>
            </Tooltip>
            <Accordion style={{borderRadius: "unset"}} className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>General</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label htmlFor="brightness">Brightness: {filters.brightness}</label>
                    <input type="range" min="0" max="2" step="0.1" name="brightness" value={filters.brightness} onChange={e => setFilters((prevState) => ({...prevState, brightness: e.target.value}))}/>
                    <br/><br/>
                    <label htmlFor="contrast">Contrast: {filters.contrast}</label>
                    <input type="range" min="0" max="2" step="0.1" name="contrast" value={filters.contrast} onChange={e => setFilters((prevState) => ({...prevState, contrast: e.target.value}))}/>
                    <br/><br/>
                    <label htmlFor="saturate">Saturate: {filters.saturate}</label>
                    <input type="range" min="0" max="2" step="0.1" name="saturate" value={filters.saturate} onChange={e => setFilters((prevState) => ({...prevState, saturate: e.target.value}))}/>
                    <br/>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon/>}
                aria-controls="panel2a-content"
                id="panel2a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Greyscales</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label htmlFor="grayscale">Greyscale: {filters.grayscale}</label>
                    <input type="range" min="0" max="1" step="0.1" name="grayscale" value={filters.grayscale} onChange={e => setFilters((prevState) => ({...prevState, grayscale: e.target.value}))}/>
                    <br/>
                    <label>Red Greyscale
                        <input type="checkbox" checked={filters.redGreyscale} onChange={e => setFilters((prevState) => ({...prevState, redGreyscale: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Green Greyscale
                        <input type="checkbox" checked={filters.greenGreyscale} onChange={e => setFilters((prevState) => ({...prevState, greenGreyscale: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Blue Greyscale
                        <input type="checkbox" checked={filters.blueGreyscale} onChange={e => setFilters((prevState) => ({...prevState, blueGreyscale: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Purple Greyscale
                        <input type="checkbox" checked={filters.purplescale} onChange={e => setFilters((prevState) => ({...prevState, purplescale: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Twenties
                        <input type="checkbox" checked={filters.twenties} onChange={e => setFilters((prevState) => ({...prevState, twenties: e.target.checked}))}/>
                    </label>
                    <br/>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel3a-content"
                id="panel3a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Noise / Specks</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <div className="filter-box">
                        <p style={{textDecoration: "underline"}}>White Noise:</p>
                        <div>
                            <label>Apply 
                                <input type="checkbox" checked={filters.whiteNoise} onChange={e => setFilters((prevState) => ({...prevState, whiteNoise: e.target.checked}))}/>
                            </label>
                        </div>
                        <div>
                            <label htmlFor="whiteNoiseVal">Value: {filters.whiteNoiseVal}</label>
                            <input type="range" min="12" max="90" step="1" name="whiteNoiseVal" value={filters.whiteNoiseVal} onChange={e => setFilters((prevState) => ({...prevState, whiteNoiseVal: e.target.value}))}/>
                        </div>
                    </div>
                    <br/>
                    <label>Specks Redscale
                        <input type="checkbox" checked={filters.specksredscale} onChange={e => setFilters((prevState) => ({...prevState, specksredscale: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Green Specks
                        <input type="checkbox" checked={filters.greenSpecks} onChange={e => setFilters((prevState) => ({...prevState, greenSpecks: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Red Casino
                        <input type="checkbox" checked={filters.redCasino} onChange={e => setFilters((prevState) => ({...prevState, redCasino: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Yellow Casino
                        <input type="checkbox" checked={filters.yellowCasino} onChange={e => setFilters((prevState) => ({...prevState, yellowCasino: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Matrix
                        <input type="checkbox" checked={filters.matrix} onChange={e => setFilters((prevState) => ({...prevState, matrix: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Eclectic
                        <input type="checkbox" checked={filters.eclectic} onChange={e => setFilters((prevState) => ({...prevState, eclectic: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Cosmic
                        <input type="checkbox" checked={filters.cosmic} onChange={e => setFilters((prevState) => ({...prevState, cosmic: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Retroviolet
                        <input type="checkbox" checked={filters.retroviolet} onChange={e => setFilters((prevState) => ({...prevState, retroviolet: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Confetti
                        <input type="checkbox" checked={filters.confetti} onChange={e => setFilters((prevState) => ({...prevState, confetti: e.target.checked}))}/>
                    </label>
                    <br/>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel4a-content"
                id="panel4a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Offsets</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label>RGB Split 
                        <input type="checkbox" checked={filters.rgbSplit} onChange={e => setFilters((prevState) => ({...prevState, rgbSplit: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <div className="filter-box">
                        <p style={{textDecoration: "underline"}}>Red Offset:</p>
                        <div>
                            <label>Apply 
                                <input type="checkbox" checked={filters.offsetRed} onChange={e => setFilters((prevState) => ({...prevState, offsetRed: e.target.checked}))}/>
                            </label>
                        </div>
                        <div>
                            <label htmlFor="offsetRedVal">Value: {filters.offsetRedVal}</label>
                            <input type="range" min="-5" max="100" step="5" name="offsetRedVal" value={filters.offsetRedVal} onChange={e => setFilters((prevState) => ({...prevState, offsetRedVal: e.target.value}))}/>
                        </div>
                    </div>
                    <br/>
                    <div className="filter-box">
                        <p style={{textDecoration: "underline"}}>Green Offset:</p>
                        <div>
                            <label>Apply 
                                <input type="checkbox" checked={filters.offsetGreen} onChange={e => setFilters((prevState) => ({...prevState, offsetGreen: e.target.checked}))}/>
                            </label>
                        </div>
                        <div>
                            <label htmlFor="offsetGreenVal">Value: {filters.offsetGreenVal}</label>
                            <input type="range" min="-100" max="100" step="5" name="offsetGreenVal" value={filters.offsetGreenVal} onChange={e => setFilters((prevState) => ({...prevState, offsetGreenVal: e.target.value}))}/>
                        </div>
                    </div>
                    <br/>
                    <div className="filter-box">
                        <p style={{textDecoration: "underline"}}>Blue Offset:</p>
                        <div>
                            <label>Apply 
                                <input type="checkbox" checked={filters.offsetBlue} onChange={e => setFilters((prevState) => ({...prevState, offsetBlue: e.target.checked}))}/>
                            </label>
                        </div>
                        <div>
                            <label htmlFor="offsetBlueVal">Value: {filters.offsetBlueVal}</label>
                            <input type="range" min="-100" max="100" step="5" name="offsetBlueVal" value={filters.offsetBlueVal} onChange={e => setFilters((prevState) => ({...prevState, offsetBlueVal: e.target.value}))}/>
                        </div>
                    </div>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel5a-content"
                id="panel5a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Inversions</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label>Invert
                        <input type="checkbox" checked={filters.invert} onChange={e => setFilters((prevState) => ({...prevState, invert: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Neue
                        <input type="checkbox" checked={filters.neue} onChange={e => setFilters((prevState) => ({...prevState, neue: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Lix
                        <input type="checkbox" checked={filters.lix} onChange={e => setFilters((prevState) => ({...prevState, lix: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Ryo
                        <input type="checkbox" checked={filters.ryo} onChange={e => setFilters((prevState) => ({...prevState, ryo: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Solange
                        <input type="checkbox" checked={filters.solange} onChange={e => setFilters((prevState) => ({...prevState, solange: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Cool Twilight
                        <input type="checkbox" checked={filters.coolTwilight} onChange={e => setFilters((prevState) => ({...prevState, coolTwilight: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Mellow
                        <input type="checkbox" checked={filters.mellow} onChange={e => setFilters((prevState) => ({...prevState, mellow: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Eon
                        <input type="checkbox" checked={filters.eon} onChange={e => setFilters((prevState) => ({...prevState, eon: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Aeon
                        <input type="checkbox" checked={filters.aeon} onChange={e => setFilters((prevState) => ({...prevState, aeon: e.target.checked}))}/>
                    </label>
                    <br/>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel6a-content"
                id="panel6a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Tints</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label>Red Effect 
                        <input type="checkbox" checked={filters.redEffect} onChange={e => setFilters((prevState) => ({...prevState, redEffect: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Perfume
                        <input type="checkbox" checked={filters.perfume} onChange={e => setFilters((prevState) => ({...prevState, perfume: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Serenity
                        <input type="checkbox" checked={filters.serenity} onChange={e => setFilters((prevState) => ({...prevState, serenity: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Pink Aura
                        <input type="checkbox" checked={filters.pinkAura} onChange={e => setFilters((prevState) => ({...prevState, pinkAura: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Radio
                        <input type="checkbox" checked={filters.radio} onChange={e => setFilters((prevState) => ({...prevState, radio: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label htmlFor="sepia">Sepia: {filters.sepia}</label>
                    <input type="range" min="0" max="1" step="0.1" name="sepia" value={filters.sepia} onChange={e => setFilters((prevState) => ({...prevState, sepia: e.target.value}))}/>
                    <br/>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel7a-content"
                id="panel7a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Opacity & Blur</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label>
                        <Tooltip arrow placement="bottom-end" title="Filter will blur motion.">
                            <InfoIcon style={{verticalAlign: "bottom"}}/>
                        </Tooltip>
                        <span> Ghost </span>
                        <input type="checkbox" checked={filters.ghost} onChange={e => setFilters((prevState) => ({...prevState, ghost: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label htmlFor="blur">Blur: {filters.blur}px</label>
                    <input type="range" min="0" max="10" step="1" name="blur" value={filters.blur} onChange={e => setFilters((prevState) => ({...prevState, blur: e.target.value}))}/>
                    <br/><br/>
                    <label htmlFor="opacity">
                        <Tooltip arrow placement="bottom-end" title="Values less than 1 can reveal a background color. Lower values will blur motion. A value of 0 effectively stops the canvas display.">
                            <InfoIcon style={{verticalAlign: "bottom"}}/>
                        </Tooltip>
                        <span> Opacity: {filters.opacity} </span>
                    </label>
                    <input style={{marginTop: "1rem"}} type="range" min="0" max="1" step="0.1" name="opacity" value={filters.opacity} onChange={e => setFilters((prevState) => ({...prevState, opacity: e.target.value}))}/>
                    <br/><br/>
                    <div className="filter-box">
                        <p style={{textDecoration: "underline"}}>
                            <Tooltip arrow placement="bottom-end" title="For this effect to be visible the 'opacity' must be less than 1 and/or the 'ghost' effect must be applied.">
                                <InfoIcon style={{verticalAlign: "bottom"}}/>
                            </Tooltip>
                            <span> Background Color: </span>
                        </p>
                        <div>
                            <label>Apply 
                                <input type="checkbox" checked={filters.bkgColor} onChange={e => setFilters((prevState) => ({...prevState, bkgColor: e.target.checked}))}/>
                            </label>
                        </div>
                        <div>
                            <label htmlFor="bkgColorVal">Value: {filters.bkgColorVal}</label>
                            <input type="color" name="bkgColorVal" value={filters.bkgColorVal} onChange={e => setFilters((prevState) => ({...prevState, bkgColorVal: e.target.value}))}/>
                        </div>
                    </div>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel8a-content"
                id="panel8a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Hues</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label htmlFor="hueRotate">Hue-rotate: {filters.hueRotate} deg</label>
                    <input type="range" min="0" max="360" step="10" name="hueRotate" value={filters.hueRotate} onChange={e => setFilters((prevState) => ({...prevState, hueRotate: e.target.value}))}/>
                    <br/><br/>
                    <label>Frontward
                        <input type="checkbox" checked={filters.frontward} onChange={e => setFilters((prevState) => ({...prevState, frontward: e.target.checked}))}/>
                    </label>
                    <br/>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel9a-content"
                id="panel9a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Gamma</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <div className="filter-box">
                        <p style={{textDecoration: "underline"}}>Full Gamma:</p>
                        <div>
                            <label>Apply
                                <input type="checkbox" checked={filters.gamma} onChange={e => setFilters((prevState) => ({...prevState, gamma: e.target.checked}))}/>
                            </label>
                        </div>
                        <div>
                            <label htmlFor="gammaVal">Value: {filters.gammaVal}</label>
                            <input type="range" min="2" max="15" step="1" name="gammaVal" value={filters.gammaVal} onChange={e => setFilters((prevState) => ({...prevState, gammaVal: e.target.value}))}/>
                        </div>
                    </div>
                    <br/>
                    <label>Red Gamma
                        <input type="checkbox" checked={filters.redGamma} onChange={e => setFilters((prevState) => ({...prevState, redGamma: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Green Gamma
                        <input type="checkbox" checked={filters.greenGamma} onChange={e => setFilters((prevState) => ({...prevState, greenGamma: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Yellow Gamma
                        <input type="checkbox" checked={filters.yellowGamma} onChange={e => setFilters((prevState) => ({...prevState, yellowGamma: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Purple Gamma
                        <input type="checkbox" checked={filters.purpleGamma} onChange={e => setFilters((prevState) => ({...prevState, purpleGamma: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Blue-Red Gamma
                        <input type="checkbox" checked={filters.blueredGamma} onChange={e => setFilters((prevState) => ({...prevState, blueredGamma: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Teal Gamma
                        <input type="checkbox" checked={filters.bluegreenGamma} onChange={e => setFilters((prevState) => ({...prevState, bluegreenGamma: e.target.checked}))}/>
                    </label>
                    <br/>
                </AccordionDetails>
            </Accordion>
            <Accordion className="acc-panel">
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel10a-content"
                id="panel10a-header"
                >
                    <Typography style={{textDecoration: "underline"}}>Other</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <label>Diagonal Lines
                        <input type="checkbox" checked={filters.addDiagonalLines} onChange={e => setFilters((prevState) => ({...prevState, addDiagonalLines: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Crimson
                        <input type="checkbox" checked={filters.crimson} onChange={e => setFilters((prevState) => ({...prevState, crimson: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Lemon
                        <input type="checkbox" checked={filters.lemon} onChange={e => setFilters((prevState) => ({...prevState, lemon: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Vintage
                        <input type="checkbox" checked={filters.vintage} onChange={e => setFilters((prevState) => ({...prevState, vintage: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Haze
                        <input type="checkbox" checked={filters.haze} onChange={e => setFilters((prevState) => ({...prevState, haze: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Horizon
                        <input type="checkbox" checked={filters.horizon} onChange={e => setFilters((prevState) => ({...prevState, horizon: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <label>Grime
                        <input type="checkbox" checked={filters.grime} onChange={e => setFilters((prevState) => ({...prevState, grime: e.target.checked}))}/>
                    </label>
                    <br/><br/>
                    <div className="filter-box">
                        <p style={{textDecoration: "underline"}}>Threshold:</p>
                        <div>
                            <label>Apply 
                                <input type="checkbox" checked={filters.threshold} onChange={e => setFilters((prevState) => ({...prevState, threshold: e.target.checked}))}/>
                            </label>
                        </div>
                        <div>
                            <label htmlFor="thresholdVal">Value: {filters.thresholdVal}</label>
                            <input type="range" min="0" max="255" step="5" name="thresholdVal" value={filters.thresholdVal} onChange={e => setFilters((prevState) => ({...prevState, thresholdVal: e.target.value}))}/>
                        </div>
                    </div>
                    <br/>
                </AccordionDetails>
            </Accordion>
        </aside>
    )
}

export default Filters;