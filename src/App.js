import React, { useState, useRef } from 'react'
import Filters from './components/Filters';
import Canvas from './components/Canvas';
import SettingsIcon from '@mui/icons-material/Settings';
import Tooltip from '@mui/material/Tooltip';

function App() {
  const [filters, setFilters] = useState({
    redEffect: false,
    rgbSplit: false,
    invert: false,
    ghost: false,
    blur: 0,
    brightness: 1,
    contrast: 1,
    grayscale: 0,
    hueRotate: 0,
    opacity: 1,
    saturate: 1,
    sepia: 0,
    threshold: false,
    thresholdVal: 100,
    bkgColor: false,
    bkgColorVal: "#0000ff",
    specksredscale: false,
    eclectic: false,
    addDiagonalLines: false,
    greenSpecks: false,
    redCasino: false,
    yellowCasino: false,
    neue: false,
    lix: false,
    ryo: false,
    solange: false,
    crimson: false,
    lemon: false,
    frontward: false,
    vintage: false,
    perfume: false,
    serenity: false,
    pinkAura: false,
    haze: false,
    coolTwilight: false,
    horizon: false,
    mellow: false,
    eon: false,
    aeon: false,
    matrix: false,
    cosmix: false,
    purplescale: false,
    radio: false,
    twenties: false,
    grime: false,
    redGreyscale: false,
    retroviolet: false,
    greenGreyscale: false,
    blueGreyscale: false,
    whiteNoise: false,
    whiteNoiseVal: 15,
    confetti: false,
    gamma: false,
    gammaVal: 2,
    bluegreenGamma: false,
    purpleGamma: false,
    yellowGamma: false,
    blueredGamma: false,
    greenGamma: false,
    redGamma: false,
    offsetGreen: false,
    offsetGreenVal: 5,
    offsetBlue: false,
    offsetBlueVal: 5,
    offsetRed: false,
    offsetRedVal: 5
  })

  const reset = () => {
    setFilters({
      redEffect: false,
      rgbSplit: false,
      invert: false,
      ghost: false,
      blur: 0,
      brightness: 1,
      contrast: 1,
      grayscale: 0,
      hueRotate: 0,
      opacity: 1,
      saturate: 1,
      sepia: 0,
      threshold: false,
      thresholdVal: 100,
      bkgColor: false,
      bkgColorVal: "#0000ff",
      specksredscale: false,
      eclectic: false,
      addDiagonalLines: false,
      greenSpecks: false,
      redCasino: false,
      yellowCasino: false,
      neue: false,
      lix: false,
      ryo: false,
      solange: false,
      crimson: false,
      lemon: false,
      frontward: false,
      vintage: false,
      perfume: false,
      serenity: false,
      pinkAura: false,
      haze: false,
      coolTwilight: false,
      horizon: false,
      mellow: false,
      eon: false,
      aeon: false,
      matrix: false,
      cosmix: false,
      purplescale: false,
      radio: false,
      twenties: false,
      grime: false,
      redGreyscale: false,
      retroviolet: false,
      greenGreyscale: false,
      blueGreyscale: false,
      whiteNoise: false,
      whiteNoiseVal: 15,
      confetti: false,
      gamma: false,
      gammaVal: 2,
      bluegreenGamma: false,
      purpleGamma: false,
      yellowGamma: false,
      blueredGamma: false,
      greenGamma: false,
      redGamma: false,
      offsetGreen: false,
      offsetGreenVal: 5,
      offsetBlue: false,
      offsetBlueVal: 5,
      offsetRed: false,
      offsetRedVal: 5
    })
  }

  const settings = useRef()

  function toggleMenu() {
    let classes = settings.current.classList
    if (classes.contains("spin-close")) {
      classes.remove("spin-close")
      classes.add("spin-open")
    } else if (classes.contains("spin-open")) {
      classes.remove("spin-open")
      classes.add("spin-close")
    } else {
      classes.add("spin-open")
    }
    const filterMenu = document.querySelector("aside")
    filterMenu.classList.toggle("reveal")
  }

  return (
    <>
      <span className="settings" ref={settings} onClick={toggleMenu}>
        <Tooltip arrow placement="bottom-end" title="Open/close filters menu.">
          <SettingsIcon fontSize='inherit'/>
        </Tooltip>
      </span>
      <Filters setFilters={setFilters} filters={filters} reset={reset}/>
      <Canvas filters={filters}/>
    </>
  );
}

export default App;
