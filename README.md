# Webcam Filters

## Description

This is a client-side, single-page application using HTML5 Canvas to apply filters to an incoming webcam video stream and record the filtered video. 

Many of the filter functions used in this app are from [pixels.js](https://github.com/silvia-odwyer/pixels.js) and the experimental [CanvasRenderingContext2D.filter](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/filter). Video recording is enabled through use of the [MediaRecorder API](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream_Recording_API). Audio is not recorded but this can be enabled - see the commented code in src/components/Canvas.js (audio quality may be poor though).

### Local Dev Environment

After cloning this repo, in your terminal (while in the relevant folder) enter the following commands to run the app locally:

`npm install`

`npm start`
